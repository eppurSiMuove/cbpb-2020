#!/usr/bin/env bash

# Mensagens do jogo ---------------------------------------

msg_chute='Chute um número de 1 a 1000: '
msg_acerto='Parabéns! Você acertou!'
msg_tentativas='Só precisou de %d tentativas...'
msg_dica="\nErrou!\nDICA:\to número"
msg_sair='Até mais!'

# Mensagens de erro ---------------------------------------

erro[1]='Eu disse um número!'
erro[2]='Eu disse de 1 a 100!'

# Mensagens de dica ---------------------------------------

dica[3]="$msg_dica é par.\n"
dica[4]="$msg_dica é impar.\n"
dica[5]="$msg_dica é primo.\n"
dica[6]="$msg_dica não é primo.\n"
dica[217]="$msg_dica é menor que isso.\n"
dica[218]="$msg_dica e maior que isso.\n"

# Funções ----------------------------------------------------
# Testa se palpite é múltiplo de 2, 3, 5 e/ou 7
# ------------------------------------------------------------
# Multiplos do nº	| Retorno da função		| Resultdo em _testaPalpite()
# ------------------------------------------------------------
# nenhum			| 1						| 7
# 2					| 2						| 8
# 3           		| 3						| 9
# 5           		| 5						| 11
# 7           		| 7						| 13
# 2, 3        		| 6						| 12
# 2, 5        		| 10					| 16
# 2, 7        		| 14					| 20
# 3, 5        		| 15					| 21
# 3, 7        		| 21					| 27
# 5, 7        		| 35					| 41
# 2, 3, 5     		| 30					| 36
# 2, 3, 7     		| 42					| 48
# 2, 5, 7     		| 70					| 76
# 3, 5, 7     		| 105					| 111
# 2, 3, 5, 7  		| 210					| 216
# ------------------------------------------------------------
_eMultiplo() {
	local -i resultado=1
	local str="$msg_dica é múltiplo de:"
	for n in 2 3 5 7; do
		if [[ $(( $1 % n )) -eq 0 ]]; then
			resultado=$(( resultado * n ))
			str="$str $n"
		fi
	done
	[[ $resultado -eq 1 ]] && str="$str nada."
	dica[$((resultado+6))]="$str\n"
	echo $resultado
	return $resultado
}

# Testa se palpite é par
# -------------------------------------------------------------
# 0 = par | 1 = impar
# -------------------------------------------------------------
_ePar() {
	return $(( $1 % 2))
}

# Testa se palpite é primo
# -------------------------------------------------------------
# 0 = primo | 1 = não é primo
# -------------------------------------------------------------
_ePrimo() {
	local -i c=0
	local -i a=$(( $1 / 2 ))
	for (( n=1; n <= $a; n++ )); do
		[[ $(( $1 % n )) -eq 0 ]] && ((c++))
	done
	[[ $c -eq 1 ]]
	return $?
}

# Testa se palpite é maior ou menor
# ------------------------------------------------------------
# 0 = maior | 1 = menor
# ------------------------------------------------------------
_maiorMenor() {
	[[ $1 -gt $SECRETO ]]
	return $?
}

# Sair...
_sair() {
	echo $msg_sair$'\n'
	exit
}

# Testar palpite...
# --------------------------------------------------------------
# Retorno		| Valor
# --------------------------------------------------------------
# 0				| sucesso, termina o programa
# 1				| erro de caractere inválido
# 2				| erro de intervalo de número
# 3-4			| dica: 3=par, 4=impar
# 5-6			| dica: 5=primo, 6=não primo
# 7-216			| dica: retorno - 6 = resultado (ver função _eMultiplo)
# 217-218		| dica: 217=menor, 218=maior
# --------------------------------------------------------------
_testaPalpite() {
	[[ $1 -eq $SECRETO ]]			&& return 0
	[[ ! $1 =~ ^[0-9]+$ ]]			&& return 1
	[[ $1 -lt 1 || $1 -gt 1000 ]]	&& return 2
	case $2 in
		1) _ePar $SECRETO;		 return $((3+$?)) ;;
		2) _ePrimo $SECRETO;	 return $((5+$?)) ;;
		3) _eMultiplo $SECRETO;	 return $((6+$?)) ;; 
		*) _maiorMenor $1;		 return $((217+$?));;
	esac		
}

# Pedir palpite...
_pedePalpite() {
	local palpite
	read -p "$msg_chute" palpite
	_testaPalpite $palpite $1
}

# Principal -----------------------------------------------

# Número SECRETO...
SECRETO=$(( ($RANDOM % 1000) + 1 ))

# Iniciar contador...
tentativas=1

# Loop infinito...
while :; do
	# Pedir palpite...
	_pedePalpite $tentativas
	resultado=$?
	case $resultado in
		0)
			echo $'\n'$msg_acerto
			printf "$msg_tentativas\n\n" $tentativas
			_sair
			;;
		*)
			echo -e ${dica[$resultado]}
			((tentativas++))
			;;
	esac
done
